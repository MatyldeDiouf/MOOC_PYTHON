#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# a shebang (+ execution rights) enables to execute the program as a command using its absolute path

from argparse import ArgumentParser
import turtle

def fibonacci(n):
    """returns fibonacci numbers of an integer n"""
    if n <= 1:
        return n
    return fibonacci(n-1) + fibonacci(n-2)

parser = ArgumentParser()
parser.add_argument(dest="entier", type=int,
                    help="entier d'entrée")
input_args = parser.parse_args()
entier = input_args.entier
# enables adding an argument directly in the command line

print(f"fibonacci({entier}) = {fibonacci(entier)}")

def square(length):
    """have the turtle draw a square of side <length>"""
    for side in range(3):
        turtle.forward(length)
        turtle.left(120)
        
turtle.reset()
square(100)