import math

def distance(*args):
    return math.sqrt(sum(i**2 for i in args))